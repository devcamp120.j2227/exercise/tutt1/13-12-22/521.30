import React from "react";
import { useDispatch, useSelector } from "react-redux";

import { Container, Grid, Input, Button, InputLabel, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Paper } from '@mui/material';

const Task = () => {
  function createData(
    stt, noidung
  ) {
    return { stt, noidung };
  }

  const drinks = [
    {
      id: 1,
      drink: 'Cà phê'
    },
    {
      id: 2,
      drink: 'Trà tắc'
    },
    {
      id: 3,
      drink: 'Pepsi'
    },
    {
      id: 4,
      drink: 'Cocacola'
    },
    {
      id: 5,
      drink: 'Trà sữa'
    },
    {
      id: 6,
      drink: 'Matcha'
    },
    {
      id: 7,
      drink: 'Hồng trà'
    },
    {
      id: 8,
      drink: 'Trà xanh kem cheese'
    },
    {
      id: 9,
      drink: 'Trà đá'
    },
  ]

  // ];
  const dispatch = useDispatch();

  const { tasks, taskName } = useSelector((reduxData) => reduxData.taskReducer);
  console.log('check', tasks);

  const rows = drinks.map((task) => {
    return (createData(task.id, task.drink))
  })

  console.log(rows);


  const taskNameHandler = event => {
    dispatch({
      type: "VALUE_HANDLER",
      payload: {
        taskName: event.target.value
      }
    });

  };

  const addTaskHandler = event => {
    event.preventDefault();
    console.log('Lọc nội dung');
    let id = 0;

    // Hàm xử lý để lấy id cho task mới
    if (tasks.length) {
      id = Math.max(...tasks.map(task => task.id));

      id++;
    }

    if (taskName) {
      dispatch({
        type: "ADD_TASK",
        payload: {
          task: {
            name: taskName,
            completed: false,
            id
          }
        }
      });
    }
  };

  console.log(drinks.filter(drink => drink.drink.toLowerCase().includes('PEP'.toLowerCase())));

  return (
    <Container>
      <Grid container spacing={2} style={{ margin: '0 auto', padding: 20 }}>
        <Grid item={true} xs={12}>
          <form onSubmit={addTaskHandler}>
            <Grid container item={true} xs={12}>
              <Grid item={true} xs={3}>
                <InputLabel htmlFor="my-input">Nhập nội dung lọc</InputLabel>
              </Grid>
              <Grid item={true} xs={5}>
                <Input value={taskName} onChange={taskNameHandler} style={{ width: '90%' }} />
              </Grid>
              <Grid item={true} xs={4}>
                <Button variant="contained" type="submit">Lọc</Button>
              </Grid>
            </Grid>
          </form>
        </Grid>
      </Grid>

      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead sx={{
            width: 300,
            color: 'blue',
            backgroundColor: '#756666',
          }}>
            <TableRow >
              <TableCell sx={{ color: 'white' }}>STT</TableCell>
              <TableCell sx={{ color: 'white' }} align="left">Nội dung</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {console.log('check', taskName)}
            {console.log('check drink', drinks)}
            {drinks.filter(drink => drink.drink.toLowerCase().includes(taskName.toLowerCase())).map(function (elem, index) {
                return (
                  <TableRow
                    key={elem.id}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {elem.id}
                    </TableCell>
                    <TableCell align="left">{elem.drink}</TableCell>

                  </TableRow>
                )
              })}

          </TableBody>
        </Table>
      </TableContainer>
    </Container>
  );
}

export default Task;